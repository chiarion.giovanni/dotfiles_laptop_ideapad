#!/bin/bash

# Check if a device is connected by bluetooth using bluetoothctl
info=$(bluetoothctl devices Connected) 

# Show some output when it is
if echo "$info" | grep -q "Device"; 
then
    # Connected to a device
    echo ' Connected'
else 
    # Not connected to a device, hide label
    echo ''
fi
